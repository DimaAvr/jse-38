package ru.tsc.avramenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.avramenko.tm.component.Bootstrap;
import ru.tsc.avramenko.tm.exception.system.ProcessException;
import ru.tsc.avramenko.tm.model.Session;
import ru.tsc.avramenko.tm.model.User;
import ru.tsc.avramenko.tm.service.ConnectionService;
import ru.tsc.avramenko.tm.service.PropertyService;
import ru.tsc.avramenko.tm.service.SessionService;

import java.util.List;

public class SessionRepositoryTest {

    @Nullable
    private SessionRepository sessionRepository;

    @Nullable
    private SessionService sessionService;

    @Nullable
    private Session session;

    @NotNull
    private Bootstrap bootstrap = new Bootstrap();

    @NotNull
    protected static final String TEST_USER_ID_INCORRECT = "647";

    @Before
    public void before() {
        ConnectionService connectionService = new ConnectionService(new PropertyService());
        sessionRepository = new SessionRepository(connectionService.getConnection());
        sessionService = new SessionService(connectionService, bootstrap);
        this.session = sessionService.open("Test", "Test");
    }

    @After
    public void after() {
        sessionService.close(session);
    }

    @Test
    public void add() {
        Assert.assertNotNull(session);
        Assert.assertNotNull(session.getId());
        Assert.assertNotNull(session.getUserId());

        @Nullable final Session sessionById = sessionRepository.findById(session.getId());
        Assert.assertNotNull(sessionById);
        Assert.assertEquals(session.getId(), sessionById.getId());
    }

    @Test
    public void findById() {
        @Nullable final Session session = sessionRepository.findById(this.session.getId());
        Assert.assertNotNull(session);
    }

    @Test
    public void findByIdIncorrect() {
        @Nullable final Session session = sessionRepository.findById(TEST_USER_ID_INCORRECT);
        Assert.assertNull(session);
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Session> session = sessionRepository.findAll();
        Assert.assertNotNull(session);
        Assert.assertTrue(session.size() > 0);
    }

    @Test
    public void removeByIdIncorrect() {
        sessionRepository.removeById(TEST_USER_ID_INCORRECT);
        Assert.assertNotNull(sessionRepository.findById(session.getId()));
    }

}