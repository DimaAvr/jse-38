package ru.tsc.avramenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.tsc.avramenko.tm.component.Bootstrap;
import ru.tsc.avramenko.tm.enumerated.Status;
import ru.tsc.avramenko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.avramenko.tm.exception.system.ProcessException;
import ru.tsc.avramenko.tm.model.Session;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.service.ConnectionService;
import ru.tsc.avramenko.tm.service.PropertyService;
import ru.tsc.avramenko.tm.service.SessionService;

import javax.validation.constraints.Null;
import java.util.List;

public class TaskRepositoryTest {

    @Nullable
    private TaskRepository taskRepository;

    @Nullable
    private SessionService sessionService;

    @NotNull
    private Bootstrap bootstrap = new Bootstrap();

    @Nullable
    private Task task;

    @Nullable
    private Session session;

    @NotNull
    protected static final String TEST_TASK_NAME = "TestName";

    @NotNull
    protected static final String TEST_DESCRIPTION_NAME = "TestDescription";

    @NotNull
    protected static final String TEST_USER_ID_INCORRECT = "TestUserIdIncorrect";

    @NotNull
    protected static final String TEST_TASK_ID_INCORRECT = "647";

    @Before
    public void before() {
        ConnectionService connectionService = new ConnectionService(new PropertyService());
        sessionService = new SessionService(connectionService, bootstrap);
        this.session = sessionService.open("Test", "Test");
        task = new Task();
        task.setName(TEST_TASK_NAME);
        task.setDescription(TEST_DESCRIPTION_NAME);
        task.setUserId(session.getUserId());
        taskRepository = new TaskRepository(connectionService.getConnection());
        task = taskRepository.add(this.session.getUserId(), task);
    }

    @After
    public void after() {
        sessionService.close(session);
    }

    @Test
    public void add() {
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getId());
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());
    }

    @Test
    public void findAll() {
        @NotNull final List<Task> tasks = taskRepository.findAll();
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void existsById() {
        Assert.assertTrue(taskRepository.existsById(session.getUserId(), task.getId()));
    }

    @Test
    public void findAllByUserId() {
        @NotNull final List<Task> tasks = taskRepository.findAll(session.getUserId());
        Assert.assertEquals(1, tasks.size());
    }

    @Test
    public void findAllByUserIdIncorrect() {
        @NotNull final List<Task> tasks = taskRepository.findAll(TEST_USER_ID_INCORRECT);
        Assert.assertEquals(0, tasks.size());
    }

    @Test
    public void findById() {
        @Nullable final Task task = taskRepository.findById(session.getUserId(), this.task.getId());
        Assert.assertNotNull(task);
    }

    @Test
    public void findByIdIncorrect() {
        @Nullable final Task task = taskRepository.findById(session.getUserId(), TEST_TASK_ID_INCORRECT);
        Assert.assertNull(task);
    }

    @Test
    public void findByIdIncorrectUser() {
        @Nullable final Task task = taskRepository.findById(TEST_USER_ID_INCORRECT, this.task.getId());
        Assert.assertNull(task);
    }

    @Test
    public void remove() {
        taskRepository.removeById(task.getId());
        Assert.assertNull(taskRepository.findById(task.getId()));
    }

    @Test
    public void findByName() {
        @Nullable final Task task = taskRepository.findByName(session.getUserId(), TEST_TASK_NAME);
        Assert.assertNotNull(task);
    }

    @Test
    public void findByNameIncorrect() {
        @Nullable final Task task = taskRepository.findByName(session.getUserId(), TEST_TASK_ID_INCORRECT);
        Assert.assertNull(task);
    }

    @Test
    public void findByNameNull() {
        @Nullable final Task task = taskRepository.findByName(session.getUserId(), null);
        Assert.assertNull(task);
    }

    @Test
    public void findByNameIncorrectUser() {
        @Nullable final Task task = taskRepository.findByName(TEST_USER_ID_INCORRECT, this.task.getName());
        Assert.assertNull(task);
    }

    @Test
    public void findByIndex() {
        @Nullable final Task task = taskRepository.findByIndex(session.getUserId(), 1);
        Assert.assertNotNull(task);
    }

    @Test
    public void removeById() {
        taskRepository.removeById(session.getUserId(), task.getId());
        Assert.assertNull(taskRepository.findById(task.getId()));
    }

    @Test
    public void removeByIdIncorrect() {
        taskRepository.removeById(session.getUserId(), TEST_TASK_ID_INCORRECT);
        Assert.assertNotNull(taskRepository.findById(task.getId()));
    }

    @Test
    public void removeByIdIncorrectUser() {
        taskRepository.removeById(TEST_USER_ID_INCORRECT, this.task.getId());
        Assert.assertNotNull(taskRepository.findById(task.getId()));
    }

    @Test
    public void removeByName() {
        taskRepository.removeByName(session.getUserId(), TEST_TASK_NAME);
        Assert.assertNull(taskRepository.findByName(session.getUserId(), TEST_TASK_NAME));
    }

    @Test
    public void removeByNameIncorrectUser() {
        taskRepository.removeByName(TEST_USER_ID_INCORRECT, this.task.getName());
        Assert.assertNotNull(taskRepository.findById(task.getId()));
    }

}